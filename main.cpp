#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>


using namespace std;

struct node {
    int num;
    double v_del;
    double e_del;
    node *next;
    node *first;
    int flg;
};

double Max_el(std::vector<double> v){
    int i = 1;
    double max = v[0];

    int size = v.size();
    while (i < size) {
        if (v[i] > max)
            max = v[i];
        i++;
    }
    return max;
}

double creat_RAT(double *RAT_count, node *Grph, int *fin, int *start, int cnt_nod, int id)
{
    std::vector<double> v;
    if (fin[id] == 1){
        return 0;
    } else if (RAT_count[id] != -1){
        return RAT_count[id];
    } else {
        node *neib = Grph[id].next;
        while (neib != NULL) {
            v.push_back(creat_RAT(RAT_count, Grph, fin, start, cnt_nod, neib->num) + Grph[neib->num].v_del + neib->e_del);
            neib = neib->next;
        }
    }
    return RAT_count[id] = Max_el(v);
}

double creat_AAT(double *AAT_count, node *Grph, int *fin, int *start, int cnt_nod, int id)
{
    std::vector<double> v;
    if (start[id] == 1){
        return 0;
    } else if (AAT_count[id] != -1){
        return AAT_count[id];
    } else {
        node *neib = Grph[id].next;
        while (neib != NULL) {
            v.push_back(creat_AAT(AAT_count, Grph, fin, start, cnt_nod, neib->num) + Grph[neib->num].v_del + neib->e_del);
            neib = neib->next;
        }
    }
    return AAT_count[id] = Max_el(v);
}

int main()
{
    node *Grph, *Gr_in;
    char str[200];
    int tmp, cnt_nod, i;


    FILE *fl_nod = fopen(".nodes.txt", "rt");
    cnt_nod = 0;
    while (fscanf(fl_nod, "%s", str) != EOF)
        cnt_nod++;
    fseek(fl_nod, 0, SEEK_SET);

    Grph = new node [cnt_nod];
    Gr_in = new node[cnt_nod];


    double vdelay;
    for (int i = 0; i < cnt_nod; ++i) {
        fscanf(fl_nod, "%lf", &vdelay);
        Gr_in[i].e_del = Grph[i].e_del = 0;
        Gr_in[i].v_del = Grph[i].v_del = vdelay;
        Gr_in[i].num = Grph[i].num = cnt_nod;
        Gr_in[i].flg = Grph[i].flg = 0;
        Gr_in[i].first = &(Gr_in[i]);
        Grph[i].first = &(Grph[i]);
        Gr_in[i].next = Grph[i].next = NULL;
    }
    fclose(fl_nod);


    FILE *fl_net = fopen(".nets.txt", "rt");
    int n1, n2;
    double delay;
    while (fscanf(fl_net, "%d%d%lf", &n1, &n2, &delay) == 3) {
        node *new_node = new node;

        new_node->num = n2;
        new_node->e_del = delay;
        new_node->v_del = Grph[n2].v_del;
        new_node->flg = 0;
        new_node->first = &(Grph[n1]);
        new_node->next = Grph[n1].next;
        Grph[n1].next = new_node;
    }
    fclose(fl_net);


    tmp = 0;
    for (i = 0; i < cnt_nod; i++) {
        node *neib = Grph[i].next;
        while (neib != 0) {
            tmp = neib->num;
            node *n = new node;

            n->num = i;
            n->e_del = neib->e_del;
            n->v_del = neib->v_del;
            n->flg = 0;
            n->next = Gr_in[tmp].next;
            n->first = &(Gr_in[tmp]);
            Gr_in[tmp].next = n;
            neib = neib->next;
        }
    }

    int *start = new int [cnt_nod], *fin = new int [cnt_nod];
    double *result = new double [cnt_nod], *RAT = new double [cnt_nod], *AAT = new double [cnt_nod];


    for (i = 0; i < cnt_nod; i++){
        start[i] = 0;
        fin[i] = 0;
        result[i] = 0;
        RAT[i] = -1;
        AAT[i] = -1;
    }


    for (i = 0; i < cnt_nod; i++){
        if (Grph[i].next == NULL) fin[i] = 1;
        if (Gr_in[i].next == NULL) start[i] = 1;
    }


    FILE *fl_aux = fopen(".aux.txt", "rt");
    while (fscanf(fl_aux, "%d", &tmp) != EOF) {
        fscanf(fl_aux, "%lf", &(RAT[tmp]));
        if (fin[tmp] != 1) {
            cerr << " error of datas in file .aux \n";
            return 1;
        }
    }
    fclose(fl_aux);


    for (i = 0; i < cnt_nod; i++)
        if (fin[i] == 1 && RAT[i] == -1) {
            cerr << " error of datas in file .aux \n";
            return 1;
        }

    for (i = 0; i < cnt_nod; i++) {
        creat_RAT(RAT, Grph, fin, start, cnt_nod, i);
        creat_AAT(AAT, Gr_in, fin, start, cnt_nod, i);
    }

    FILE *fl_sl = fopen(".slacks.txt", "wt");
    tmp = 0;
    for (i = 0; i < cnt_nod; i++) {
        result[i] = RAT[i] - AAT[i];
        fprintf(fl_sl, "%.1lf\n", result[i]);
        if (result[i] < 0) tmp++;
    }
    fclose(fl_sl);
    FILE *fl_res = fopen(".results.txt", "wt");
    if (tmp == 0)
        fprintf(fl_res, "0\n");
    else {
        fprintf(fl_res, "1\n");
        for (i = 0; i < cnt_nod; i++){
            if (result[i] < 0)
                fprintf(fl_res, "%d ", i);
        }
    }
    fclose(fl_res);

    return 0;
}
